#! /bin/bash

#dialog installer that explains what will happen & create $USER, with option to cancel

#some constant values used by the dialog screens to make them more legible
BACK="--colors --backtitle"
TITLE="\Z7Avalanche Conversion Tool"
MSG='--yes-label "OK" --msgbox'
SIZE="10 75"
SIZE2="20 75"

dialog  $BACK "$TITLE" --title "WELCOME!" $MSG  "Welcome to the Avalanche Conversion Tool.\n\nThis utility will make a number of (mostly reversible) changes to your system. It should be run directly after installing Alpine Linux 3.16, which will setup the foundation we build upon." $SIZE

dialog $BACK "$TITLE" --form "Please supply the required information:\n(username can only contain small letters)" $SIZE 3 "Full Name:" 1 0 "" 1 11 60 0 "username :" 2 0 "" 2 11 60 0 2>input && FULLNAME=$(sed -n '1p' input) && USER=$(sed -n '2p' input | sed 's/[A-Z]/\L&/g') && rm -f input
#Return status of non-zero indicates cancel
if [ "$?" != "0" ] || [ "$USER" = "" ] || [ "$FULLNAME" = "" ]
then
  dialog $BACK "$TITLE" $MSG "Avalanche Conversion was canceled at your request or either field was empty.\n\nPlease restart the Avalanche Conversion Tool." $SIZE && clear
else

dialog $BACK "$TITLE" --insecure --title "Password Provider" --passwordbox "Provide a password for $FULLNAME" $SIZE 2>password && PASSWORD1=$(sed -n '1p' password) && rm -f password
#Return status of non-zero indicates cancel
  if [ "$?" != "0" ] || [ "$PASSWORD1" = "" ]
  then
    dialog $BACK "$TITLE" $MSG "Avalanche Conversion was canceled at your request or input field was empty.\n\nPlease restart the Avalanche Conversion Tool." $SIZE && clear
  else

    dialog $BACK "$TITLE" --insecure --title "Password Provider" --passwordbox "Confirm password for $FULLNAME" $SIZE 2>password && PASSWORD2=$(sed -n '1p' password) && rm -f password
#Return status of non-zero indicates cancel
    if [ "$?" != "0" ] || [ "$PASSWORD2" = "" ]
    then
      dialog $BACK "$TITLE" $MSG "Avalanche Conversion was canceled at your request or input field was empty.\n\nPlease restart the Avalanche Conversion Tool." $SIZE && clear
    else

      if [ "$PASSWORD1" != "$PASSWORD2" ]
      then
        dialog $BACK "$TITLE" $MSG "Avalanche Conversion was canceled because the provided passwords didn't match.\n\nPlease restart the Avalanche Conversion Tool." $SIZE && clear
      else
        dialog $BACK "$TITLE" --title "Confirmation" --yes-label "Enter" --no-label "Esc" --yesno "This is the last opportunity to cancel the conversion.\n\nPlease press <Enter> to start conversion or <Esc> to cancel." $SIZE

#Return status of non-zero indicates cancel
        if [ "$?" != "0" ]
        then
          dialog $BACK "$TITLE" $MSG "Avalanche Conversion was canceled at your request." $SIZE && clear
        else
#backup rc.conf & enable rc_parallel
          cp /etc/rc.conf /etc/rc.conf.bak
          sed -i 's/\#rc\_parallel\=\"NO\"/rc\_parallel\=\"YES\"/g' /etc/rc.conf

#backup & switch repositories config file to https connection. enable community repo.
          cp /etc/apk/repositories /etc/apk/repositories.bak
          sed -i 's/http/https/g' /etc/apk/repositories
          sed -i 's/\# //g' /etc/apk/repositories
#enable all repositories for Lumina
          sed -i 's/\#h/h/g' /etc/apk/repositories

#backup original useradd config file & switch default shell from ash to bash.
          mkdir /etc/default
          touch /etc/default/useradd
          echo -e 'SHELL=/bin/bash' > /etc/default/useradd
#add aliases to bash to ease transition to doas & make 'rm' always interactive.
          echo -e '#! /bin/bash\n\nalias sudo="doas"\nalias rm="rm -i"' > /etc/profile.d/aliases.sh
          mv /etc/profile.d/color_prompt.sh.disabled /etc/profile.d/color_prompt.sh

          adduser -D -g "$FULLNAME" $USER -s /bin/bash
          echo -e "$PASSWORD1\n$PASSWORD2" | passwd $USER

          apk upgrade | dialog $BACK "\Z7Avalanche Conversion Tool  -  Installing upgrades" --progressbox $SIZE2
          setup-xorg-base | dialog $BACK "\Z7Avalanche Conversion Tool  -  Installing Xorg Base" --progressbox $SIZE2
          apk add doas htop nano firefox flatpak elogind polkit-elogind pm-utils | dialog $BACK "\Z7Avalanche Conversion Tool  -  Installing Base applications" --progressbox $SIZE2
#         apk add plasma kde-applications-base kde-applications-accessibility | dialog $BACK "$TITLE  - Installing KDE Plasma" --progressbox $SIZE2
#         apk add lumina-desktop sddm-breeze breeze-grub breeze-gtk breeze-plymouth breeze-icons qterminal | dialog $BACK "\Z7Avalanche Conversion Tool  -  Installing Lumina Desktop" --progressbox $SIZE2
#         apk add lumina-desktop lightdm-gtk-greeter breeze breeze-grub breeze-gtk breeze-plymouth breeze-icons qterminal | dialog $BACK "\Z7Avalanche Conversion Tool  -  Installing Lumina Desktop" --progressbox $SIZE2
#         apk add mate-desktop-environment dbus dbus-x11 lxdm adwaita-icon-theme faenza-icon-theme | dialog $BACK "\Z7Avalanche Conversion Tool  -  Installing Mate Desktop" --progressbox $SIZE2
          apk add lxqt-desktop sddm-breeze | dialog $BACK "$TITLE  - Installing LXQt Desktop" --progressbox $SIZE2
          apk add $(apk search -q ttf- | grep -v '\-doc' | grep -v 'opensans') | dialog $BACK "\Z7Avalanche Conversion Tool  -  Installing True Type Fonts" --progressbox $SIZE2
#backup original doas.conf and permit group wheel and add some persistence
          cp /etc/doas.d/doas.conf /etc/doas.d/doas.conf.bak
          echo -e "permit :wheel\npermit persist :wheel" >> /etc/doas.d/doas.conf
#add video driver for QEMU
          apk add xf86-video-modesetting xf86-video-intel | dialog $BACK "$TITLE  -  Installing QEMU video drivers" --progressbox $SIZE
#         apk add xf86-qxl | dialog $BACK "$TITLE  -  Installing QEMU video drivers" --progressbox $SIZE
#backup nanorc & change nanorc settings to be a bit more advanced
          cp /etc/nanorc /etc/nanorc.bak
          wget https://gitlab.com/avalanche-linux/avalanche-linux/-/raw/main/nanorc
          mv nanorc /etc/
          wget https://gitlab.com/avalanche-linux/avalanche-linux/-/raw/main/.nanorc
          mv .nanorc /root/

#add & enable Internet Wireless Device (iwd)
          apk add iwd
          rc-service iwd start
          cp /etc/NetworkManager/NetworkManager.conf /etc/NetworkManager/NetworkManager.conf.bak
          echo -e "\n[device]\nwifi.backend=iwd" >> /etc/NetworkManager/NetworkManager.conf

#add pipewire and its plumbing
#https://wiki.alpinelinux.org/wiki/PipeWire
          apk add pipewire wireplumber pipewire-alsa pipewire-pulse pipewire-jack pipewire-spa-bluez pipewire-media-session
          mkdir /etc/pipewire
          cp /usr/share/pipewire/pipewire.conf /etc/pipewire/
          sed -i 's/#{ path = "\/usr\/bin\/pipewire" args = "-c pipewire-pulse.conf" }/{ path = "\/usr\/bin\/pipewire" args = "-c pipewire-pulse.conf" }\n    { path
 = "wireplumber"  args = "" }/g' /etc/pipewire/pipewire.conf
          modprobe snd_seq
          echo snd_seq >> /etc/modules

#enable Suspend on LID close
          mkdir /etc/acpi/LID
          touch /etc/acpi/LID/00000080
          echo -e "#!/bin/sh\n\nexec pm-suspend" >/etc/acpi/LID/00000080
          chmod +x /etc/acpi/LID/00000080
#add user to relevant groups
          addgroup $USER audio
          addgroup $USER disk
          addgroup $USER flatpak
          addgroup $USER input
          addgroup $USER video
          addgroup $USER wheel
#add sddm to OpenRC to automatically start it at boot
          rc-update add sddm
#         rc-update add udev
#         rc-update add dbus
#         rc-update add lxdm
#         rc-update add lightdm
#add Flathub to Flatpak and install Flatseal to graphically edit permissions
          doas -u $USER flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
          doas -u $USER flatpak install com.github.tchx84.Flatseal -y
          dialog $BACK "$TITLE" --title "Finished!" --yes-label "Enter" --no-label "Esc" --yesno "Conversion was completed, it is recommended to reboot to finish your new Desktop experience!\n\nPlease press <Enter> to reboot or <Esc> to continue your current session." $SIZE
          if [ "$?" != "0" ]
          then
            clear
          else
            clear && reboot now
          fi
        fi
      fi
    fi
  fi
fi
