# Avalanche Linux

 Avalanche Linux wants to create a community that wants to build desktop experiences with Alpine Linux!

- [x] 🏁 Our first goal is to have an install script, because that is something I have experience with already (and should therefor be attainable before the end of the 2021! (even thou it'll be a duplicate effort in the long run)). It probably won't have all the bells and whistles we'd like to have shipped in our final™ product.

- [ ] 🏁 Our second goal will be creating an .iso with a live environment; first it'll include that install script but later it'll convert to use the Calamares installer. It is a very modular installer AFAIK, so that'll be perfect for this project (and far simpler to maintain than the initial install script presumably 🤞😎)

- [ ] 🏁 Inspired by macOS's `TimeMachine` feature to restore individual files, one of the more ambitious goals of this project is to have a more granular `Btrfs` setup. This should provide the ability to restore individual folders, instead of the entire /home folder hierarchy.

Technical possibilities/limitations aside, but the current line of thought is to create `@USER_Downloads`...`@USER_Videos`...`@USER_Music` etc subvolumes. This should all be automatically set up and hopefully be integrated into the file manager 🤞😬

Because this project is aimed at end-users (and we don't want to make too many choices for them), Flatpak (w/Flathub) will be preconfigured.  Snaps would be nice, but aren't a possibility right now.  But we'll keep it under the radar for addition when it will be, and time permitted we'll actively engage in enabling this for our users!

If people have issues with them being installed they can simply be remove by technical users, but for everyone else it'll be much easier to install the apps they want because we've broadened the app availability to them 😏
