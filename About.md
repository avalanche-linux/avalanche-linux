# How we got our name

Initially I was thinking of calling it Kilimanjaro Linux, because would I thought `desktop experiences` + `Alpine Linux` = `tabletop mountain`.  But I assumed that either people would think it was a different Linux project (aka `Manjaro Linux`) or the indigenous people around that mountain wouldn't like some small project using that name.

But in a thread started on Mastodob, someone came with the suggestion of Pyrenee Linux, which I quite liked.

For those interested in the history of this project, you can find the thread here: https://fosstodon.org/@RyuKurisu/107237076480507977
